package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private TextView resultado, nUsuario;
    private EditText numeroUno, numeroDos;
    private Button botonSumar, botonRestar, botonMultiplicar, botonDividir, botonRegresar, botonLimpiar;
    private Calculadora operacion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        numeroUno = findViewById(R.id.txtNumA);
        numeroDos = findViewById(R.id.txtNumB);
        botonSumar = findViewById(R.id.btnSuma);
        botonRestar = findViewById(R.id.btnResta);
        botonMultiplicar = findViewById(R.id.btnMulti);
        botonDividir = findViewById(R.id.btnDivi);
        resultado = findViewById(R.id.lblResultado);
        botonRegresar = findViewById(R.id.btnRegresar);
        botonLimpiar = findViewById(R.id.btnLimpiar);
        nUsuario =findViewById(R.id.lblUsuario);
        operacion = new Calculadora();
        Bundle datos = getIntent().getExtras();
        String user = datos.getString("Usuario");
        nUsuario.setText("Usuario : " + user);


        botonRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        botonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             numeroUno.setText("");
             numeroDos.setText("");
             resultado.setText("0");
            }
        });


        View.OnClickListener commonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (isValidFloat(numeroUno.getText().toString()) && isValidFloat(numeroDos.getText().toString()) ) {

                    operacion.setNumA(Float.parseFloat(numeroUno.getText().toString()));
                    operacion.setNumB(Float.parseFloat(numeroDos.getText().toString()));

                    if (view.equals(botonSumar)) {
                        resultado.setText(String.valueOf(operacion.sumar()));
                    } else if (view.equals(botonRestar)) {
                        resultado.setText(String.valueOf(operacion.restar()));
                    } else if (view.equals(botonMultiplicar)) {
                        resultado.setText(String.valueOf(operacion.multiplicar()));
                    } else if (view.equals(botonDividir)) {
                        resultado.setText(String.valueOf(operacion.dividir()));
                    }

                }

                else   {
                        Toast.makeText(CalculadoraActivity.this, "Ingrese un valor valido", Toast.LENGTH_SHORT).show();
                }


            }
        };

        botonSumar.setOnClickListener(commonOnClickListener);
        botonRestar.setOnClickListener(commonOnClickListener);
        botonMultiplicar.setOnClickListener(commonOnClickListener);
        botonDividir.setOnClickListener(commonOnClickListener);

    }

    private boolean isValidFloat(String input) {
        try {
            Float.parseFloat(input);
            return true; // si la conversion es exitosa retorna true
        } catch (NumberFormatException e) {
            // crashea
            return false;
        }
    }

}