package com.example.calculator;

public class Calculadora {
    public float numA, numB;
    public float res;

    public Calculadora(){

        this.setNumA(0.0f);
        this.setNumB(0.0f);
    }

    public void setNumA(float numA) {this.numA = numA;}
    public void setNumB(float numB) {this.numB = numB;}

    public float sumar(){
        return this.numA + this.numB;
    }
    public float restar(){
        return this.numA - this.numB;
    }
    public float multiplicar(){
        return this.numA * this.numB;
    }
    public float dividir(){
        return this.numA / this.numB;
    }

}
