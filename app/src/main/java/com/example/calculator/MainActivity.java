package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText userBox, passBox;
    private Button btnLog, btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userBox = findViewById(R.id.txtUsuario);
        passBox = findViewById(R.id.txtPass);
        btnLog = findViewById(R.id.btnIngresar);
        btnClose = findViewById(R.id.btnSalir);

        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(userBox.getText().toString().trim().equals(getText(R.string.user)) && passBox.getText().toString().trim().equals(getText(R.string.pass))  ){

                    Intent calculos = new Intent(MainActivity.this, CalculadoraActivity.class);
                    calculos.putExtra("Usuario", userBox.getText().toString());
                    startActivity(calculos);

                }

                else {
                    Toast.makeText(MainActivity.this, "Datos incorrectos: intente de nuevo", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}